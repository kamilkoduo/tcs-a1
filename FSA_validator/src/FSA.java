import exceptions.*;
import javafx.util.Pair;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Class for FSA containing:
 * Set of states, alphabet, set of final states as simply arrays of strings;
 * initial state as String;
 * transitions as HashMap<<State, Alpha>,State>
 */
public class FSA {
    private String[] states;
    private String[] alpha;
    private String initState;
    private String[] finalStates;
    private HashMap<Pair<String, String>, String> transitions;

    private boolean complete;

    private String[] parseStrings(String input, String key) throws MalformedFileException {
        boolean beg = (input.substring(0, key.length() + 2).equals(key + "={"));
        boolean end = (input.substring(input.length() - 1, input.length()).equals("}"));

        if (!beg || !end) throw new MalformedFileException();

        return input.substring(key.length() + 2, input.length() - 1).split(",");
    }

    private HashMap<Pair<String, String>, String> parseTransitions(String[] strings) throws MalformedFileException, AlphaException, StateException {
        HashMap<Pair<String, String>, String> transitions = new HashMap<>();
        for (String trans : strings) {
            String[] temp = trans.split(">");
            if (temp.length != 3) throw new MalformedFileException();
            validateState(temp[0]);
            validateAlpha(temp[1]);
            validateState(temp[2]);
            transitions.put(new Pair<>(temp[0], temp[1]), temp[2]);
        }
        return transitions;
    }

    private void validateState(String state) throws StateException {
        for (String st : this.states) {
            if (st.equals(state)) {
                return;
            }
        }
        throw new StateException();
    }

    private void validateAlpha(String alpha) throws AlphaException {
        for (String al : this.alpha) {
            if (al.equals(alpha)) {
                return;
            }
        }
        throw new AlphaException();
    }


    private HashSet dfs(HashMap<Pair<String, String>, String> transitions, String initState) {
        HashSet usedStates = new HashSet();
        Stack<String> stack = new Stack();
        stack.push(initState);
        while (!stack.isEmpty()) {
            String v = stack.pop();
            if (!usedStates.contains(v)) {
                usedStates.add(v);
                for (String symbol : this.alpha) {
                    String state = transitions.get(new Pair(v, symbol));
                    if (state == null) {
                        this.complete = false;
                    } else if (!usedStates.contains(state)) {
                        stack.push(state);
                    }
                }
            }
        }
        return usedStates;
    }

    private void validateIsJoint() throws DisjointStatesException {
        HashMap transitions = new HashMap();
        for (Map.Entry<Pair<String, String>, String> entry : this.transitions.entrySet()) {
            transitions.put(entry.getKey(), entry.getValue());
            transitions.put(new Pair(entry.getValue(), entry.getKey().getValue()), entry.getKey().getKey());
        }
        HashSet usedStates = dfs(transitions, states[0]);
        for (String state : states) {
            if (!usedStates.contains(state))
                throw new DisjointStatesException();
        }

    }


    public FSA(String input) throws FSAException {
        String[] strings = input.split("\n");
        if (strings.length != 5) throw new MalformedFileException();

        this.states = parseStrings(strings[0], "states");
        this.alpha = parseStrings(strings[1], "alpha");

        try {
            this.initState = parseStrings(strings[2], "init.st")[0];
        } catch (IndexOutOfBoundsException e) {
            throw new InitialStateIsNotDefinedException();
        }

        this.finalStates = parseStrings(strings[3], "fin.st");
        this.transitions = parseTransitions(parseStrings(strings[4], "trans"));

        this.complete = true;

        validateState(initState);
        validateIsJoint();
    }


    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("src/fsa.txt"));
        String s = "";
        while (scanner.hasNextLine())
            s += scanner.nextLine() + "\n";

        try {
            System.out.println(s);

            FSA fsa = new FSA(s);


            System.out.println(Arrays.toString(fsa.states) + "\n" +
                    Arrays.toString(fsa.alpha) + "\n" +
                    fsa.initState + "\n" +
                    Arrays.toString(fsa.finalStates) + "\n" +
                    Arrays.toString(fsa.transitions.entrySet().toArray()));

        } catch (MalformedFileException e) {
            System.out.println(e.getMessage());
        }
    }

}