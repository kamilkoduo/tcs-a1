package exceptions;

public class MalformedFileException extends FSAException {
    public MalformedFileException(){
        super("E5: Input file is malformed");
    }
}
