package exceptions;

public class InitialStateIsNotDefinedException extends FSAException {
    public InitialStateIsNotDefinedException(){
        super("E4: Initial state is not defined");
    }
}
