package exceptions;

public class AlphaException extends FSAException{
public AlphaException() {
        super("E3: A transition a is not represented in the alphabet" );
    }

}
