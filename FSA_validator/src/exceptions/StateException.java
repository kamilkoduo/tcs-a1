package exceptions;

public class StateException extends FSAException{
    public StateException() {
        super("E1: A state s is not in set of states" );
    }

}
