package exceptions;

public class FSAException extends Exception {
    public FSAException(String message){
        super("FSA Exception: "+ message);
    }
}
