package exceptions;

public class DisjointStatesException extends FSAException {
    public DisjointStatesException() {
        super("E2: Some states are disjoint");
    }
}
