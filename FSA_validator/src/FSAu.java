import exceptions.AlphaException;
import exceptions.InitialStateIsNotDefinedException;
import exceptions.MalformedFileException;
import exceptions.StateException;
import javafx.util.Pair;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class FSAu {

    private HashSet<String> states;
    private HashSet<String> alpha;
    private String initState;
    private HashSet<String> finalStates;

    private HashMap<String, HashMap<String,HashSet<String>>> transitions;

    private String[] parseStrings(String input, String key) throws MalformedFileException {
        boolean beg = (input.substring(0, key.length() + 2).equals(key + "={"));
        boolean end = (input.substring(input.length() - 1, input.length()).equals("}"));

        if (!beg || !end) throw new MalformedFileException();

        return input.substring(key.length() + 2, input.length() - 1).split(",");
    }

    private HashSet<String> parseStates(String input) throws MalformedFileException {
        String[] parsedStates = parseStrings(input, "states");

        /*
        for(String state: parsedStates){
            for(Character symbol)
        }
        */
        return new HashSet<>(Arrays.asList(parsedStates));
    }

    private HashSet<String> parseAlpha(String input) throws MalformedFileException {
        String[] parsedAlpha = parseStrings(input, "alpha");

        /*
        for(String state: parsedStates){
            for(Character symbol)
        }
        */

        return new HashSet<>(Arrays.asList(parsedAlpha));
    }

    private String parseInit(String input) throws MalformedFileException, InitialStateIsNotDefinedException, StateException {
        String[] init = parseStrings(input, "init.st");

        if (init.length == 0)
            throw new InitialStateIsNotDefinedException();

        if (!getStates().contains(init[0])) throw new StateException();

        return init[0];
    }

    private String[] parseFinalStates(String input) throws MalformedFileException, StateException {
        String[] finalStates = parseStrings(input, "fin.st");

        if (finalStates.length == 0) {//warning1
        }

        for (String finalState : finalStates)
            if (!getStates().contains(finalState)) throw new StateException();

        return finalStates;
    }

    private HashMap<String, HashMap<String,HashSet<String>>> parseTransitions(String input) throws MalformedFileException, AlphaException, StateException {
        String[] trans = parseStrings(input, "trans");

        HashMap<String, HashMap<String,HashSet<String>>> transitions = new HashMap<>();

        for (String tr : trans) {
            String[] temp = tr.split(">");
            if (temp.length != 3) throw new MalformedFileException();

            if (!getAlpha().contains(temp[1])) throw new AlphaException();
            if (!getStates().contains(temp[0])) throw new StateException();
            if (!getStates().contains(temp[2])) throw new StateException();

            transitions.putIfAbsent(temp[0], new HashMap<>());
            transitions.get(temp[0]).putIfAbsent(temp[1],new HashSet<>());
            transitions.get(temp[0]).get(temp[1]).add(temp[2]);

        }
        return transitions;
    }




    public HashSet<String> getStates() {
        return states;
    }

    public void setStates(HashSet<String> states) {
        this.states = states;
    }

    public HashSet<String> getAlpha() {
        return alpha;
    }

    public void setAlpha(HashSet<String> alpha) {
        this.alpha = alpha;
    }

}


//disjoint
//complete incomplete
//w2 some states are not reacheable from initial state
//w3 fsa is non deterministic